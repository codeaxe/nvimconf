This directory contains settings for various nvim plugins and nvim settings.

## Plugin Settings
Each plugin's overrides/settings should be put in a separate file named `{plugin-name}.vim` under `${HOME}/.config/nvim/plugins/`.
## Nvim Settings
General vim overrides/settings should be put in a separate file named `settingname.vim` under `${HOME}/.config/nvim/settings/`.

### Install Instructions:
```bash
sh -c "`curl -fsSl https://bitbucket.org/codeaxe/nvimconf/raw/d4e5873016c079b4df638ac955c176703a0b73e2/install.sh`"
```
##Note:
This will be removed and merged to [zmesh](https://github.com/git4sroy/zmesh.git) in future.
